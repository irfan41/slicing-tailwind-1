const { pink } = require('tailwindcss/colors')
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './*.html',
    './src/css/*.styles.css',
 ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'display': ['Helvetica', 'Arial', 'sans-serif'],
    },
    colors: {
      // Build your palette here
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.coolGray,
      red: colors.red,
      yellow: colors.amber,
      blue: colors.blue,
      sky: colors.sky,
      purple: colors.purple,
      pink: colors.pink,
      rose: colors.rose,
      orange: colors.orange,
      green: colors.green
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
